package com.fd.textrecognition;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    static final String TAG = "DBG_" + MainActivity.class.getName();
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 7;
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS = 8;
    private static final int MY_START_FILE_SELECT = 122;
    TextView txView;
    Button btnSelect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txView = findViewById(R.id.txView);
        btnSelect = findViewById(R.id.btnSelectFile);

        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sImei = "";
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) sImei = ReadIMEI();
                if (false && ValidateIMEI(sImei)) {
                    Toast.makeText(MainActivity.this, "IMEI=" + sImei+"\n"+"SN="+ReadSerialNumber(), Toast.LENGTH_LONG).show();
                } else {
                    txView.setText("");
                    Intent intent = new Intent()
                            .setType("image/*")
                            .setAction(Intent.ACTION_GET_CONTENT);

                    startActivityForResult(Intent.createChooser(intent, "Select Screenshot file"), MY_START_FILE_SELECT);
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Explain to the user why we need to read the contacts
                }

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }

        }
    }

    private String ReadSerialNumber() {
        String sSerialNubmer = "";
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                sSerialNubmer = android.os.Build.SERIAL;
            } else {
                if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    sSerialNubmer = android.os.Build.getSerial();
                }
            }
        }

        return sSerialNubmer;
    }

    private String ReadIMEI(){
        String sImei="";
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE)) {
                    // Explain to the user why we need to read the contacts
                }

                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},
                        MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS);
            } else {
                sImei = telephonyManager.getDeviceId();
            }
        } else {
            sImei = telephonyManager.getDeviceId();
        }
        return sImei;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[]
            permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED) {

                } else {
                    //权限未授予
                    Toast.makeText(this, "在未授予权限的情况下，程序无法正常工作",
                            Toast.LENGTH_SHORT).show();
                }
            }
            break;
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    String sImei = ReadIMEI();
                    if (!ValidateIMEI(sImei)) {
                        txView.setText("");
                        Intent intent = new Intent()
                                .setType("image/*")
                                .setAction(Intent.ACTION_GET_CONTENT);

                        startActivityForResult(Intent.createChooser(intent, "Select Screenshot file"), MY_START_FILE_SELECT);
                    } else {
                        Toast.makeText(MainActivity.this, "IMEI=" + sImei, Toast.LENGTH_LONG).show();
                    }
                } else {
                    //权限未授予
                    Toast.makeText(this, "在未授予权限的情况下，程序无法正常工作",
                            Toast.LENGTH_SHORT).show();
                }
            }
            break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == MY_START_FILE_SELECT && resultCode==RESULT_OK)
        {
            Uri selectedimg = data.getData();
            //imageView.setImageBitmap(MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedimg));
            Bitmap bmp = null;
            try {
                bmp = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedimg);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(bmp==null) return;

            TextRecognizer textRecognizer = new TextRecognizer.Builder(this).build();

            if (!textRecognizer.isOperational()) {
                new AlertDialog.Builder(this)
                        .setMessage("Text recognizer could not be set up on your device :(")
                        .show();
                return;
            }

            Frame frame = new Frame.Builder().setBitmap(bmp).build();
            SparseArray<TextBlock> text = textRecognizer.detect(frame);

            for (int i = 0; i < text.size(); ++i) {
                TextBlock item = text.valueAt(i);
                if (item != null && item.getValue() != null) {
                    String s = item.getValue();
                    txView.append(s+"  "+item.getBoundingBox());
                    //txView.setText(item.getValue());
                    String array1[]= s.split("\n");
                    for (String temp: array1){
                        if (ValidateIMEI(temp)) {
                            Toast.makeText(MainActivity.this, "IMEI=" + temp, Toast.LENGTH_LONG).show();
                        }
                    }
                    txView.append(System.getProperty("line.separator"));
                }
            }

        }
    }

    private Boolean ValidateIMEI(String sImei){
        Boolean bFind = false;
        if (TextUtils.isEmpty(sImei)) return bFind;
        if (sImei.length() == 15){
            int sum = 0;
            boolean errorflag = false;
            for (int i = 0; i <= 14; i++) {
                //getting ascii value for each character
                char c = sImei.charAt(i);
                int number = c;
                //Assigning number values to corrsponding Ascii value
                if (number < 48 || number > 57) {
                    errorflag = true;
                    break;
                } else
                {
                    switch (number) {
                        case 48:
                            number = 0;
                            break;
                        case 49:
                            number = 1;
                            break;
                        case 50:
                            number = 2;
                            break;
                        case 51:
                            number = 3;
                            break;
                        case 52:
                            number = 4;
                            break;
                        case 53:
                            number = 5;
                            break;
                        case 54:
                            number = 6;
                            break;
                        case 55:
                            number = 7;
                            break;
                        case 56:
                            number = 8;
                            break;
                        case 57:
                            number = 9;
                            break;
                    }
                    //Double the even number and divide it by 10. add quotient and remainder
                    if ((i + 1) % 2 == 0) {
                        number = number * 2;
                        number = number / 10 + number % 10;
                    }
                    sum = sum + number;
                }
            }
            // Check the error flag to avoid overWriting of Warning Lable
            if (!errorflag) {
                if (sum % 10 == 0) {
                    bFind = true;
                }
            }
        }
        return bFind;
    }
}
